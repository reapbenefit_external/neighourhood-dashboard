import { Component, OnInit, ElementRef, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ward } from '../services/wards';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {

  @ViewChild('bar') bar: ElementRef;
  @ViewChild('pie') pie: ElementRef;

  @Input() mapData;
  @Input() graphData;
  @Input() compare;
  @Input() showgraphs;
  @Input() graphDataStatus;
  @Input() showComp;
  @Input() AQMGraphLoc;
  @Input() AQM;
  @Input() selIssCat;
  public MapData;
  public GraphData;
  public GraphData1;
  public GraphData2;
  public Compare;
  public showGraphs = false;
  public GraphDataStatus;
  public Wards;
  public ShowComp = false;
  public AQMGraphLocAll;
  public aqm;
  public SelIssCat;
  public ward1Selected;
  public ward2Selected;
  constructor(private dataService: DataService) { }

  config = {
    displayKey: "name", //if objects array passed which key to be displayed defaults to description
    search: true, //true/false for the search functionlity defaults to false,
    height: '250px', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
    placeholder: 'Select Ward', // text to be displayed when no item is selected defaults to Select,
    customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
    limitTo: 500, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
    moreText: 'more', // text to be displayed whenmore than one items are selected like Option 1 + 5 more
    noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
    searchPlaceholder: 'Search Ward', // label thats displayed in search input,
    searchOnKey: 'name', // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
  }

  ngOnInit() {
    this.Wards = ward;
    this.Wards.sort(this.dynamicSort("name"));
  }

  dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }
  @Output() WardData = new EventEmitter();
  public wardDataEvent;
  wardDetails(event) {
    this.wardDataEvent = event;
    this.WardData.emit(this.wardDataEvent);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.MapData = this.mapData;
    this.GraphData = this.graphData;
    this.Compare = this.compare;
    this.showGraphs = this.showgraphs;
    this.GraphDataStatus = this.graphDataStatus;
    this.ShowComp = this.showComp;
    this.AQMGraphLocAll = this.AQMGraphLoc;
    this.aqm = this.AQM
    this.SelIssCat = this.selIssCat
    this.showCompare = false;
    this.ward2Selected = undefined;
    this.ward1Selected = undefined;
    this.ward1Data = undefined;
    this.ward2Data = undefined;
  }

  showPie = true;
  ChartView(type) {
    if (type == "Bar") {
      this.showPie = true;
    } else {
      this.showPie = false;
    }
  }

  private showCompare = false;
  compareMethod(type) {
    this.showCompare = !this.showCompare;
  }

  private ward1;
  compareSelected1(event) {
    this.multi = [];
    if (event.value.id != undefined) {
      this.ward1 = event.value.id;
      let obj = {
        "cityId": this.dataService.SelectedCity,
        "wardId1": this.ward1,
        "category": this.SelIssCat == undefined ? undefined : this.SelIssCat
      }
      this.dataService.compareReport(obj).subscribe(data => {
        console.log(data);
        this.wardData = data;
        this.GraphData1 = this.wardData.graph.category;
        for (let i = 0; i < this.GraphData1.length; i++) {
          let obj = {
            "name": this.GraphData1[i].value,
            "value": this.GraphData1[i].ward1count
          }
          this.multi.push(obj);
        }
        this.GraphData = this.multi;
        this.ward1Data = this.wardData.wardOne.wardOneDetails;
      },
        error => {
          console.log(error);
        });
    }
  }

  private statusSelected;
  StatusSelected(event) {
    this.statusSelected = event
  }

  public ward2;
  public wardData;
  public multi = [];
  public multi2 = []
  public compareGraph;
  public ward1Data;
  public ward2Data;
  compareSelected2(event) {
    this.multi2 = [];
    if (event.value.id != undefined) {
      this.ward2 = event.value.id;
      let obj = {
        "cityId": this.dataService.SelectedCity,
        "wardId1": this.ward2,
        "category": this.SelIssCat == undefined ? undefined : this.SelIssCat
      }
      this.dataService.compareReport(obj).subscribe(data => {
        console.log(data);
        this.wardData = data;
        this.GraphData2 = this.wardData.graph.category;
        for (let i = 0; i < this.GraphData2.length; i++) {
          let obj2 = {
            "name": this.GraphData2[i].value,
            "value": this.GraphData2[i].ward1count
          }
          this.multi2.push(obj2);
        }
        this.GraphDataStatus = this.multi2;
        this.ward2Data = this.wardData.wardOne.wardOneDetails;
      },
        error => {
          console.log(error);
        });
    }
  }
}

