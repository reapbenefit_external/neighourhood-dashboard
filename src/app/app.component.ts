import { Component, OnInit, Input, SimpleChanges, NgZone, ViewChild, ElementRef } from '@angular/core';
import { DataService } from './services/data.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { } from 'googlemaps';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { FormControl } from '@angular/forms';

declare const google: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  // @ViewChild('Governance') Governance: ElementRef;

  title = 'Neighbourhood';
  private showMenu = true;
  public cities = [{
    "id": 1,
    "name": "Bangalore",
    "lat": 12.9796,
    "lng": 77.5906
  }, {
    "id": 2,
    "name": "Mumbai",
    "lat": 18.9690,
    "lng": 72.8205
  }, {
    "id": 4,
    "name": "Delhi",
    "lat": 28.6643,
    "lng": 77.2167
  }, {
    "id": 36,
    "name": "Chennai",
    "lat": 13.0822,
    "lng": 80.2755
  }, {
    "id": 6,
    "name": "Tumkur",
    "lat": 13.3392,
    "lng": 77.1140
  }];
  private Wards = ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4'];
  public categories = [{
    "id": 1,
    "value": 'Air',
    "ImgSrc": "./assets/Icons/Air.png",
    "checked": false
  }, {
    "id": 6,
    "value": 'Waste',
    "ImgSrc": "./assets/Icons/Waste.png",
    "checked": false
  }, {
    "id": 9,
    "value": 'Potholes',
    "ImgSrc": "./assets/Icons/Pothole.png",
    "checked": false
  }, {
    "id": 7,
    "value": 'Water',
    "ImgSrc": "./assets/Icons/Water.png",
    "checked": false
  }, {
    "id": 2,
    "value": 'Energy',
    "ImgSrc": "./assets/Icons/Energy.png",
    "checked": false
  }, {
    "id": 4,
    "value": 'Sanitation',
    "ImgSrc": "./assets/Icons/Sanitation.png",
    "checked": false
  }, {
    "id": 5,
    "value": 'Traffic',
    "ImgSrc": "./assets/Icons/Traffic.png",
    "checked": false
  }, {
    "id": 3,
    "value": 'Other',
    "ImgSrc": "./assets/Icons/Others.png",
    "checked": false
  }];
  private AQMS = ['GOVT', 'RB'];
  public close = false;
  private agencyData;
  private DWCCData;
  private toiletsData;
  private campaignsData;
  public mapData = [];
  public graphData = [];
  public compare = true;
  public mapType;
  public showComp = false;

  constructor(private dataService: DataService,
    private modalService: NgbModal,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }


  SelectedCategory(event) {
    console.log(event);
    for (let ic = 0; ic < this.categories.length; ic++) {
      if (this.categories[ic].value == event) {
        this.categories[ic].checked = !this.categories[ic].checked;
      }
    }
    this.IssueSer();
  }
  open(content,type) {
    let size
    if(type == 'cityData'){
      size = "sm";
    }else{
      size = "lg";
    }
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: size }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    });
  }

  private issGraph;
  public graphDataStatus = [];
  public selIssCat;
  public issueMapData;
  private Governance;

  IssueSer() {
    this.showComp = true;
    this.graphDataStatus = [];
    this.showgraphs = false;
    this.AQM = false;
    (<HTMLInputElement>document.getElementById('DWCC')).checked = false;
    (<HTMLInputElement>document.getElementById('Toilet')).checked = false;
    (<HTMLInputElement>document.getElementById('Campaigns')).checked = false;
    (<HTMLInputElement>document.getElementById('AQMcheck')).checked = false;
    (<HTMLInputElement>document.getElementById('ICatagory')).checked = true;
    (<HTMLInputElement>document.getElementById('Governance')).checked = false;
    (<HTMLInputElement>document.getElementById('Agencies')).checked = false;
    this.mapData = [];
    let catArr = [];
    this.graphData = [];
    let issObj;
    for (let i = 0; i < this.categories.length; i++) {
      if (this.categories[i].checked == true) {
        catArr.push(this.categories[i].id);
      }
    }
    this.dataService.catType = 2;
    issObj = {
      "cityId": this.SelectedCity,
      "category": catArr
    }
    this.selIssCat = catArr;
    this.dataService.getReports(issObj).subscribe(data => {
      console.log(data);
      this.issueMapData = data;
      this.mapData = this.issueMapData.mapData;
      this.issGraph = data;
      this.mapData.forEach(element => {
        if (element.category == "Waste") {
          element.icon = './assets/Icons/Waste.png'
        } else if (element.category == "Pothole") {
          element.icon = './assets/Icons/Pothole.png'
        } else if (element.category == "Water") {
          element.icon = './assets/Icons/Water.png'
        } else if (element.category == "Energy") {
          element.icon = './assets/Icons/Energy.png'
        } else if (element.category == "Traffic") {
          element.icon = './assets/Icons/Traffic.png'
        } else if (element.category == "Sanitation") {
          element.icon = './assets/Icons/Sanitization.png'
        }
      });
      for (let ig = 0; ig < this.issGraph.graph.category.length; ig++) {
        var obj = {
          "name": this.issGraph.graph.category[ig].value,
          "value": this.issGraph.graph.category[ig].count
        }
        this.graphData.push(obj);
      }
      for (let ig = 0; ig < this.issGraph.graph.status.length; ig++) {
        var obj = {
          "name": this.issGraph.graph.status[ig].value,
          "value": this.issGraph.graph.status[ig].count
        }
        this.graphDataStatus.push(obj);
      }
      // for (let ig = 0; ig < this.issGraph.graph.subCategory.length; ig++) {
      //   var obj = {
      //     "name": this.issGraph.graph.subCategory[ig].name,
      //     "value": this.issGraph.graph.subCategory[ig].count
      //   }
      //   this.graphData.push(obj);
      // }
    });
  }

  SelectedAQM(event) {
    console.log(event);
  }

  SelectWard(event) {
    console.log(event);
  }

  public SelectedCity = 1;
  SelectCity(event) {
    if(event != 1){
    (<HTMLInputElement>document.getElementById('dataWarning')).click();
    }
    this.mapData = [];
    this.showgraphs = false;
    console.log(event);
    this.SelectedCity = event;
    for (let sc = 0; sc < this.cities.length; sc++) {
      if (this.cities[sc].id == event) {
        this.dataService.SelectedCity = this.cities[sc].id;
        this.dataService.SelectedCityLat = this.cities[sc].lat;
        this.dataService.SelectedCityLng = this.cities[sc].lng;
      }
    }
    var obj = {
      "lat": this.dataService.SelectedCityLat,
      "lng": this.dataService.SelectedCityLng
    }
    var arr = [];
    arr.push(obj);
    this.mapData = arr;
    for (let clr = 0; clr < this.categories.length; clr++) {
      this.categories[clr].checked = false;
    }
    this.dataService.AQMDataList = undefined;
    this.AQM = false;
    this.dataService.catType = 0;
    (<HTMLInputElement>document.getElementById('DWCC')).checked = false;
    (<HTMLInputElement>document.getElementById('Toilet')).checked = false;
    (<HTMLInputElement>document.getElementById('Campaigns')).checked = false;
    (<HTMLInputElement>document.getElementById('AQMcheck')).checked = false;
    (<HTMLInputElement>document.getElementById('ICatagory')).checked = false;
    (<HTMLInputElement>document.getElementById('Governance')).checked = false;
    (<HTMLInputElement>document.getElementById('Agencies')).checked = false;
  }

  private dwcGraph;
  private AQMData;
  public AQM = false;
  radioType(event, type) {
    this.showComp = false;
    this.AQM = false;
    this.graphDataStatus = [];
    this.showgraphs = false;
    for (let n = 0; n < this.categories.length; n++) {
      this.categories[n].checked = false;
    }
    this.mapData = [];
    this.graphData = [];
    console.log(event);
    let obj = {
      "cityId": Number(this.SelectedCity)
    }
    if (type == 'Agencies') {
      this.dataService.catType = 5;
      this.compare = true;
      this.dataService.getAgencies(obj).subscribe(data => {
        this.agencyData = data;
        this.mapData = this.agencyData.agencies;
        this.mapData.forEach(element => {
          if (element.name == "Mini Bangalore One Centre" || element.name == "Bangalore One Centre" || element.name == "Kiosk") {
            element.icon = './assets/Icons/BOne.png'
          } else if (element.name == "BBMP Revenue Office") {
            element.icon = './assets/Icons/BB.png'
          } else if (element.name == "Zonal Office") {
            element.icon = './assets/Icons/BWSSB.png'
          } else if (element.name == "BESCOM office" || element.name == "ELCITA Office") {
            element.icon = './assets/Icons/BESCOM.png'
          } else if (element.name == "Police Station") {
            element.icon = './assets/Icons/Police.png'
          }
        });
        for (let ag = 0; ag < this.agencyData.total_count.length; ag++) {
          var obj = {
            "name": this.agencyData.total_count[ag].name,
            "value": this.agencyData.total_count[ag].count
          }
          this.graphData.push(obj);
        }
      }, error => {
        console.log(error);
      })
    } else if (type == 'Issues Categories') {
      this.dataService.catType = 2;
      this.compare = true;
      this.showComp = true;
    } else if (type == 'DWCC') {
      this.dataService.catType = 7;
      this.compare = true;
      this.dataService.getDWCC(obj).subscribe(data => {
        this.DWCCData = data;
        this.mapData = this.DWCCData.data;
        this.dwcGraph = data;
        this.mapData.forEach(element => {
          if (element.facility_type == "Bio Methanation Unit (BMU)") {
            element.icon = './assets/Icons/BioMethanation.png'
          } else if (element.facility_type == "Dry Waste Collection Center (DWCC)") {
            element.icon = './assets/Icons/DryWaste.png'
          } else if (element.facility_type == "Centralized Processing Facility") {
            element.icon = './assets/Icons/ProcessFacility.png'
          } else if (element.facility_type == "Leaf Litter Processing Units (LLPU)") {
            element.icon = './assets/Icons/Leaf.png'
          } else if (element.facility_type == "Organic Waste Converter (OWC)") {
            element.icon = './assets/Icons/OrgWaste.png'
          } else if (element.facility_type == "Landfills") {
            element.icon = './assets/Icons/LandFills.png'
          }
        });
        for (let ig = 0; ig < this.dwcGraph.total_count.length; ig++) {
          var obj = {
            "name": this.dwcGraph.total_count[ig].name,
            "value": this.dwcGraph.total_count[ig].count
          }
          this.graphData.push(obj);
        }
      }, error => {
        console.log(error);
      })
    } else if (type == "Governance") {
      this.dataService.catType = 6;
      this.compare = true;
      this.dataService.getGovernance(obj).subscribe(data => {
        this.Governance = data;
        this.mapData = this.Governance.Corporator;
        this.mapData.forEach(element => {
          element.icon = './assets/Icons/Corporator.png'
        });
        this.mapData = this.mapData.concat(this.Governance.MLA)
        this.mapData.forEach(element => {
          if (element.icon == undefined) {
            element.icon = './assets/Icons/MLA.png'
          }
        });
        for (let gg = 0; gg < this.Governance.total_count.length; gg++) {
          var obj = {
            "name": this.Governance.total_count[gg].name,
            "value": this.Governance.total_count[gg].count
          }
          this.graphData.push(obj);
        }
      }, error => {
        console.log(error);
      })
    } else if (type == 'Toilets') {
      this.dataService.catType = 8;
      this.compare = true;
      this.dataService.getToilets(obj).subscribe(data => {
        this.toiletsData = data;
        this.mapData = this.toiletsData.data;
        this.mapData.forEach(element => {
          element.icon = './assets/Icons/Toilet.png'
        });
        for (let to = 0; to < this.toiletsData.info.length; to++) {
          var obj = {
            "name": this.toiletsData.info[to].name,
            "value": this.toiletsData.info[to].count
          }
          this.graphData.push(obj);
        }
      }, error => {
        console.log(error);
      })
    } else if (type == 'Campaigns') {
      this.dataService.catType = 1;
      this.compare = false;
      this.dataService.getCampaigns(obj).subscribe(data => {
        this.campaignsData = data;
        this.mapData = this.campaignsData.mapData;
        this.mapData.forEach(element => {
          element.icon = './assets/Icons/Camp.png'
        });
        for (let cm = 0; cm < this.campaignsData.graph.category.length; cm++) {
          var obj = {
            "name": this.campaignsData.graph.category[cm].value,
            "value": this.campaignsData.graph.category[cm].count
          }
          this.graphData.push(obj);
        }
        for (let cm1 = 0; cm1 < this.campaignsData.graph.status.length; cm1++) {
          var obj = {
            "name": this.campaignsData.graph.status[cm1].value,
            "value": this.campaignsData.graph.status[cm1].count
          }
          this.graphDataStatus.push(obj);
        }
      }, error => {
        console.log(error);
      })
    } else if (type == 'AQM') {
      this.dataService.catType = 9;
      this.compare = false;
      this.AQM = true;
      this.dataService.AQMdata(obj).subscribe(data => {
        this.AQMData = data;
        this.mapData = this.AQMData.data;
        this.mapData.forEach(element => {
          if (element.source == 1) {
            element.icon = './assets/Icons/AQMGovt.png'
          } else {
            element.icon = './assets/Icons/AQMPvt.png'
          }
        });
        this.AQMGraphLoc = this.AQMData.graph;

        this.dataService.AQMDataList = this.AQMData.data;
      },
        error => {
          console.log(error);
        });
    }
  }

  public AQMGraphLoc;
  public status = false;
  public showgraphs = false;
  closeSideNav() {
    this.status = !this.status;
  }
  showGraphs() {
    this.showgraphs = !this.showgraphs
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.dataService.SelectedCityLat = position.coords.latitude;
        this.dataService.SelectedCityLng = position.coords.longitude;
      });
    }
  }

  public searchControl: FormControl;

  @ViewChild("search") searchElementRef: ElementRef;
  public AQMDataLoad;
  ngOnInit() {
    let obj = {
      "cityId": this.SelectedCity
    }
    this.dataService.AQMdata(obj).subscribe(data => {
      this.AQMDataLoad = data;
      this.AQMDataLoad.data.sort(this.dynamicSort("name"));
      this.dataService.AQMDataList = this.AQMDataLoad.data;
    },
      error => {
        console.log(error);
      });

    //create search FormControl
    this.searchControl = new FormControl();

    this.setCurrentPosition();
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {        
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.dataService.SelectedCityLat = place.geometry.location.lat();
          this.dataService.SelectedCityLng = place.geometry.location.lng();
          var warNmObj = {
            "lat": place.geometry.location.lat(),
            "lng": place.geometry.location.lng()
          }
          this.dataService.getCorrLocWard(warNmObj).subscribe(data => {
            this.currentWard = data;
            this.wardName = this.currentWard.details[0].name;
            this.wardID = this.currentWard.details[0].id;
          })
        });
      });
    });
  }
  public wardName;
  public wardID;
  public currentWard;
  public wardDetails;

  WardData(event) {
    this.wardName = event.name;
    this.wardID = event.id;
  }

  getlocation() {
    navigator.geolocation.getCurrentPosition(pos => {
      // this.dataService.getCorrLocWard(pos.coords.latitude, pos.coords.longitude).subscribe(data => {
      //   this.wardDetails = data;
      var currLoc = {
        'lat': pos.coords.latitude,
        'lng': pos.coords.longitude,
        'icon': './assets/Icons/myLocation.png'
        // 'wardName':this.wardDetails[0].ward_name
      }
      var obj = []
      obj.push(currLoc);
      this.mapData = obj;
      this.dataService.SelectedCityLat = 0;
      this.dataService.SelectedCityLng = 0;
      setTimeout(() => {
        this.dataService.SelectedCityLat = pos.coords.latitude;
        this.dataService.SelectedCityLng = pos.coords.longitude;
      }, 100);
      this.showgraphs = false;
      for (let clr2 = 0; clr2 < this.categories.length; clr2++) {
        this.categories[clr2].checked = false;
      }
      (<HTMLInputElement>document.getElementById('DWCC')).checked = false;
      (<HTMLInputElement>document.getElementById('Toilet')).checked = false;
      (<HTMLInputElement>document.getElementById('Campaigns')).checked = false;
      (<HTMLInputElement>document.getElementById('AQMcheck')).checked = false;
      (<HTMLInputElement>document.getElementById('ICatagory')).checked = false;
      (<HTMLInputElement>document.getElementById('Governance')).checked = false;
      (<HTMLInputElement>document.getElementById('Agencies')).checked = false;
    }, error => {
      console.log(error);
      // });
    });
  }

  cordDetails(event) {
    console.log(event);
  }
  closemenu(event) {
    if (event == true) {
      this.showMenu = true;
    } else {
      this.showMenu = false;
    }
  }
  dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }
  public showClear = false;
  searchText(event) {
    this.searchControl.value;
    if (this.searchControl.value.length > 0) {
      this.showClear = true;
    } else {
      this.showClear = false;
    }
  }
  ClearSearch() {
    this.searchControl.setValue('');
    this.showClear = false;
  }
}
