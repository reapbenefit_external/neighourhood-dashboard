import { Component, OnInit, Input, SimpleChanges, NgZone, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { DataService } from '../services/data.service';
import { FormControl } from '@angular/forms';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-maps-component',
  templateUrl: './maps-component.component.html',
  styleUrls: ['./maps-component.component.css']
})
export class MapsComponentComponent implements OnInit {

  @ViewChild('infoWindow') infoWindow: ElementRef

  @Input() MapData;
  @Output() wardDetails = new EventEmitter();
  public zoom: number = 12;
  // initial center position for the map    
  public lat: any = 13.04501680979963;
  public lng: any = 77.60573693017929;

  public infoWindowOpened = null
  public previous_info_window = null
  public markers = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.locationDetails = {
      "id": '',
      "ward_id": '',
      "wardName": '',
      "name": '',
      "type": '',
      "streetName": '',
      "address": "",
      "contactNum": "",
      "manager_name": "",
      "facility_type": '',
      "category": '',
      "status": '',
      "provider_website": '',
      "gents": '',
      "ladies": '',
      "zone": '',
      "source": '',
      "constituency": '',
      "party": '',
      "cityName": '',
      "email": '',
      "wards": '',
      "description": '',
      "campaignEnd": '',
      "url": '',
      "DateOfCompliant": '',
      "userId": '',
      "complaintNumber": '',
      "NoOfLikes": ''
    }
  }

  ngDoCheck() {
    this.lat = this.dataService.SelectedCityLat;
    this.lng = this.dataService.SelectedCityLng;
  }

  public previous;
  select_marker(data) {
    if (this.previous_info_window == null)
      this.previous_info_window = data;
    else {
      this.infoWindowOpened = data;
      this.previous_info_window.close();
    }
    this.previous_info_window = data;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.markers = this.MapData;
    this.previous_info_window = null;
    this.locationDetails = {
      "id": '',
      "ward_id": '',
      "wardName": '',
      "name": '',
      "type": '',
      "streetName": '',
      "address": "",
      "contactNum": "",
      "manager_name": "",
      "facility_type": '',
      "category": '',
      "status": '',
      "provider_website": '',
      "gents": '',
      "ladies": '',
      "zone": '',
      "source": '',
      "constituency": '',
      "party": '',
      "cityName": '',
      "email": '',
      "wards": '',
      "description": '',
      "campaignEnd": '',
      "url": '',
      "DateOfCompliant": '',
      "userId": '',
      "complaintNumber": '',
      "NoOfLikes": ''
    }
  }

  public locationDetails;
  public responceData;
  clickedMarker(marker, index: number) {
    let obj;
    console.log(`clicked the marker: ${marker.name || index}`)
    if (this.dataService.catType != 9) {
      obj = {
        "type": this.dataService.catType,
        "id": Number(marker.id)
      }
    } else {
      obj = {
        "type": this.dataService.catType,
        "id": Number(marker.stationId)
      }
    }
    this.dataService.getCorrLocDetails(obj).subscribe(data => {
      console.log(data);
      this.responceData = data;
      this.locationDetails = this.responceData.data;
    });
  }

  mapClicked(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    let obj = {
      "type": 5,
      "id": 2
    }
    this.dataService.getCorrLocDetails(obj).subscribe(data => {
      console.log(data);
    });
  }

  markerDragEnd(m, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  public currentWard;
  centerChange(event: any) {
    // console.log(event);
    var warNmObj = {
      "lat": event.lat,
      "lng": event.lng
    }
    this.dataService.getCorrLocWard(warNmObj).subscribe(data => {
      this.currentWard = data;
      this.wardDetails.emit(this.currentWard.details[0]);
    })
  }

}
